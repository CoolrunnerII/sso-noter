\header {
  title = "Sinfonia"
  composer = "G.Ph Telemann"
  instrument = "Viola"
   
}
\version "2.18.2"

num =
#(define-music-function (parser location musique) (ly:music?)
    #{ 
      \override Score.BarNumber.break-visibility = ##(#f #t #t)
      $musique 
      \revert Score.BarNumber.break-visibility 
    #})

\score{
\relative c'
{ 
    
  
% Increase the size of the bar number by 2
\override Score.BarNumber.font-size = #2

% Draw a box round the following bar number(s)
\override Score.BarNumber.stencil = #(make-stencil-boxer 0.1 0.25 ly:text-interface::print)

\time 3/2
\clef alto
\key c \major
\compressFullBarRests

e2\ff c r4 e4\upbow | f2 d r4 d\upbow | e2 f a\upbow | gis1-3( gis4)\fermata

\bar "||"

e'-4-.\upbow \break \time 4/4  \bar ""

\num{  c-.-2 c-. c-. c-. }| c-. c-. c-. c-. | e,-. e-. e-. e-. | e2( c4) a |\num{ c c c c} | \break

c c c c | e e e e | c2 r4 e\upbow |\num{ a8[\< a] a[ a] a[ a] a[ a]\!}| \break

g4\f g\> g g | g8[\mp g]\< g[ g] g[ g] g[\! g]| a4\f a\> f f | \num{f8[\mp f]\< f[ f] f[ f] f[ f]\!} \break  

e4\f e\> e e | e8[\mp e]\< e[ e] e[ e] e[ e] |e4\f e\> c a' | \num{a8[\mp a]\< a[ a] a[ a] a[ a]\!} \break

g4\f g\> g g | g8[\mp g]\< g[ g] g[ g] g[ g] | f4\f f\> f f | \num{f8[\mp f]\< f[ f] f[ f] f[ f]\!}  \break

e4\f e\> e e | e8[\mp e]\< e[ e] e[ e] e[ e] | d[ d] d[ d]\! c[ c] c[ c] | \num{c4\ff c d d }\break

c4\downbow a8[\downbow a] a[ a] a[ a] | a2\p f' | a1 | c,2 e 2 | a1 | \num{b,2 d} | f1 | a,2 c | \break

c1 | \num{e2\mf a} | c1 | d,2 f | b1 | d,2 f\< | b1 | \num{a4->\ff\downbow e-> a-> g-> }| \break

f8[-> f] f[ f] f[ f] f[ f] | g4-> d-> g-> f-> | e8[-> e] e[ e] e[ e] e[ e] | \num{f4-> c-> f-> e->} | \pageBreak

d8[-> d] d[ d] d[ d] d[ d] | e4-> b-> e-> d-> | c8[-> c] c[ c] c[ c] c[ c] | \num{a'4-> e-> a-> g->} \break

f8[-> f] f[ f] f[ f] f[ f] | g4-> d-> g-> f-> | e8[-> e] e[ e] e[ e] e[ e] | \num{f4-> c-> f-> e->} \break

d8[-> d] d[ d] d[ d] d[ d] | e4-> b-> e-> d-> | e8[->\< e] e[ e] e[ e] e[ e]\! |\break

d[\< d] d[ d] c[ c]\! c[ c] | c4\f c d gis,-1 | c2(\> c4) c'\f\upbow | \num{c-. c-.c-.c-.} | \break

c-. c-. c-. c-.|  e,-. e-. e-. e-. | e2( c4) a'\upbow |\num{ c c c c} | c c c c |\break

e, e e e | c2 r 4 c'\upbow | \num{a8[ a] a[ a] a[ a] a[ a]} | g4 g g g| \break

g8[ g] g[ g] g[ g] g[ g] | a4 a f f | \num{f8[ f] f[ f] f[ f] f[ f]} | e4 e e e | \break

e8[\mp e]\< e[ e] e[ e] e[ e] | d[ d] d[ d] c[ c]\! c[ c] | \num{a'8[\ff a] a[ a] a[ a] a[ a]} | \break

c8[ c] c[ c] c[ c] c[ c] | d8[-> d] d[ d] d[ d] d[ d] | b[ b] b[ b] b[ b] b[ b] | c2->\downbow r | c->\downbow r | a,2.->\downbow r4

\bar "|."
}


}
