\header {
  title = "Pomp and Circumstance"
  composer = "Edward Elagar"
  copyright = "Tor Berg"
  instrument = "Viola"
  
}
\version "2.18.2"

\score 
{
\relative c'
{ 
\time 4/4
\clef alto
\key c \major
\set Score.markFormatter = #format-mark-circle-numbers
\compressFullBarRests

c'2\f  b8( c d4) | a2 g | f e8( f) g4 | c2(\upbow\dim b)\! | g2(\p g4)\fermata g\upbow\p

\repeat volta 2 {
    g,2 c | a d4( cis) |\break
    d4--\< d8-- d--\!\> d2\! | d4( c2) c4 | b d^0 dis4.^1 dis8 | e2 e4( dis) | g, g c b | a2. a4\mf \mark \default | g a b g | \break
    c c b2 | b4 b c2 | b\> a\! | g4\p g g a8( b) |c2. b8(a) | g4 c d(^4 c)| {s4 \< s b1\!\upbow\> s4 \! s}|| \break 
    c2\p b | c c | a g4\upbow c\downbow | c2(\upbow g) | g a\< | g\> b\! | c c | b1 \mark \default | c4 c b b | \break
    c2 c | a4\upbow a g c | a2(\upbow g) | g\downbow\cresc a\! | d d |c4\f f f8( e d4) | {s2\> e1\upbow s2\!} |f2\p g, \break
    c a | a^"Allargando" g | 
}
    \alternative {
        { c2. c'4\p_"A tempo" | f,2\< a\!^4| g b,4(\> a\!) | a(\dim g)\! g2 | g2.\pp\fermata g'4\break}
        { c,4 c\< e c'\!}
    }
    c4.\f\downbow^"Grandioso" c8\upbow b( c) d4 | c2 c | c c4 g | fis2\upbow( g) | e\< d\! | g g | c c8(\> b4 a8)\! \break
      {s2\< b1\upbow s2\!} \mark \default | c4\ff\upbow c b8( c) d4| c2 c | f,4\upbow f g8( f) e4 | a2\upbow^4( g) | g,\downbow d' | g d | c'4^> c^> b^> b^> \break
      c^> g g g \mark \default | c2 g | g a^4 | f4^"Allagrando" f b b\upbow | c2\downbow g | c e | c4 r <g g,>\downbow r | <g g,>1\downbow\fermata
    
}
\header {
    piece = "Maestoso"
}

}
