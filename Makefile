all: Rondo\ in\ G.pdf Sinfonia\ -\ Telemann.pdf Selections\ from\ Carmen.pdf Pomp\ and\ Circumstance.pdf Menuett.pdf

Rondo\ in\ G.pdf: Rondo\ in\ G.ly
	lilypond "$<"

Sinfonia\ -\ Telemann.pdf: Sinfonia\ -\ Telemann.ly
	lilypond "$<"

Selections\ from\ Carmen.pdf: Selections\ from\ Carmen.ly
	lilypond "$<"

Pomp\ and\ Circumstance.pdf: Pomp\ and\ Circumstance.ly
	lilypond "$<"

Menuett.pdf: Menuett.ly
	lilypond "$<"
