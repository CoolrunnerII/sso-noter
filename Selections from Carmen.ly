\book{
    \paper {
    print-all-headers = ##t
  }
    \header {
    title = "Selections from Carmen"
    
    tagline = ##f
}
    \version "2.18.2"

    \score{
    \relative c'
    { 
    \time 2/4
    \clef alto
    \key d \minor
    \compressFullBarRests

    r2 | r4 d8^\markup { \center-align {\italic pizz.} } d8 |

    \repeat volta 2 {
        d8 r d d | d r a a | a r a a | \break
        b r d d | d r d d | d r d d | cis r g g
    }
    \alternative {
        {f' r d d }
        {\key d \major
            fis r d' cis}
    }

    \bar "||"\break

    c r b bes | a r gis g | fis r g fis | e r d' cis | c r b bes | \break 
    a r g fis | e r fis e | d r fis a | \bar "||" d, r d a' | fis r d e | \break
    fis r g fis | e r g, b| e r e b | g r  e' fis | g r a g | \break 
    fis[ r16 a16-0^\markup { \center-align {\italic arco }}] \ff d8-.-1 e8-.-2 | fis-3 r d,^\markup { \center-align {\italic pizz. }} \p a'| fis r d e |
    fis r b a | fis[ r16 a16-0^\markup { \center-align {\italic arco }}] \ff d8-.-1 e8-.-2 | \break
    fis-3 r e,^\markup { \center-align {\italic pizz. }} \p b | g' r e fis | \arpeggioBracket <a, e'>\f\arpeggio r r4\fermata | r d8\mf e | \bar"||" \break
    fis r fis e | d r fis g | a r b a | g r e fis | g r g fis | \break e r g a 
    g r e fis | g r g fis | e r g a | \break   
    b r cis b | a[ r16 a16-0^\markup { \center-align {\italic arco }}] \ff d8-.-1 e8-.-2 |
    fis-3 r d,^\markup { \center-align {\italic pizz. }}-.\p a'-. | 
    fis r d e |fis r b-. a-. |  \break
    e[ r16 a16-0^\markup { \center-align {\italic arco }}] \ff d8-.-1 e8-.-2 |
    fis-3 r e,^\markup { \center-align {\italic pizz. }}\p b | g r e fis | <a e'>\f\arpeggio r r4\fermata |
    <fis' a>8[^\markup { \center-align {\italic arco }} r16 a,\ff] d8 r | \bar"||" 



    }
    \header {
    title = "1. Habanera"
      instrument = "Viola"
    composer = "Georges Bizet"
       }

    }


    \score{
    \relative c'
    { 
    \time 2/4
    \clef alto
    \key a \major
    \compressFullBarRests

    \set Score.currentBarNumber = #95
    
    \repeat volta 2 {
        a8\ff e' e d | a e' e d | a e' g, e' |fis dis e d| \break
        d fis a, e' | d fis a, e' | d fis d f | cis eis eis d | \break
        a e' e d | a e' e d | a e' g, e' | fis dis e d | c e g, e' \break
        c e g,  
        \mark \markup { \right-align {"To Coda " \musicglyph #"scripts.coda" }}
        e'
    \bar"||" e d16 d d 8 d16 d| d8 d16 d cis 8 r
    }
    
    <a c>\mp r <a c> r | \break
    <a c> r <a c>\> r | <a c>\p r <a c> r | <a c> r <a c>\> r |<a c>\pp r <a c> r | <a c> r <a c> r | \break
    <a c> r <a c> r | <a c> r <a c> r | <bes d> r <bes e> r | <a c> r <a c> r | <a c> r <b f> r | \break
    
    <c e> r <c e> r | <g bes> r <g bes> r | <g bes> r <g bes> r | <a d> r <a d> r | <a d> r <a d> r |\break
    <a c> r <a c> r | <a d> r <gis d> r | <a cis> r <a cis> r | <a cis> r <a cis> r |<a fis'> r <ais d> r | \break
    
    <b d> r <d fis> r | e \tuplet 3/2 {c'16([ d c)]} a8-. fis-. | e cis16-. cis-. a8-. fis'-. | e \tuplet 3/2{ a,16\ff[( b a])} e'8-. d-. | cis-. r <e gis>-.-\markup {\right-align {\italic{"div."}}} r^\markup { \center-align {"D.C. al Coda"}} | \break
    \bar "||"
      % The coda
   \mark \markup { \musicglyph #"scripts.coda" " Coda" }

   c'16(\ff g) e-. g-. c16( g) e-. g-. | c16( g) e-. g-. c16( g) e-. g-. | e8-. e16-. e-. \tuplet 3/2 {e8-. e-. e-.} | \tuplet 3/2 {e8-. e-. e-.} e-. gis-. |  a r a r
    
    \bar "||"
    }
    \header {
    title = "3. Les Toréadors"
         instrument = "Viola"   
    piece = "Allegro giocoso"
       }

    }
}
