\header {
  title = "2. Rondo in G"
  composer = "W.A. Mozart"
  instrument = "Viola"
}
\version "2.18.2"

\score{
\relative c'
{ 
\time 4/4
\clef alto
\key g \major
\partial 2
\compressFullBarRests


\repeat volta 2 {
    b\downbow\p b | c8( b) a g c4 c | c2 e4 e4 | fis8( e) d c fis4 fis | fis2 b,4 b4 | \break 
     c8( b) a g g'4 r4 |  r1 | r2 c,2\startTrillSpan | b4\stopTrillSpan r4 }
\repeat volta 2 {
    r2 |  a2 a | a4 r4 r2| \break
    d2. b'4 | a2 r2 | a,2\mf a |  a r | r4 d4 a' a, | a2 r \break
    r4 b\downbow r gis\upbow| r a r a | r a r a | r g r2 | R1*2 |  d'1(\f |\break 
    d4) r b\downbow\p b |  c8( b) a g c4 c| c2 e4 e | fis8( e) d c fis4 fis| fis2 b,4 b | \break
    c8( b) a g g'4 r | r1 | r2 c,2\startTrillSpan | 
    \once \override Score.RehearsalMark.font-size = #3
    \mark \markup { to Coda }

    
    b4\stopTrillSpan r
    \once \override Score.RehearsalMark.font-size = #4
    \mark \markup { \musicglyph #"scripts.coda" }
    
}
\repeat volta 2 {
    r2 | R1 | d2\upbow\p bes'( | bes) a |  \break  
    bes r | r1 | a2 a,4 a |  a'8( g) f2 e4 | d2
}

d4\upbow\f d8 d8 | f4( b,) b b8 b8 | \break

d4( c) c c8 c8 | es4( a,) a a8 a | c4 bes r2 | r2 d(\downbow\p |d) g,| r2 c'(| \break
c) bes4\mp bes8 bes | a2 g4 g8 g | fis2 d4\f d8 d8 | d2 g,2 | a4 d2( b4) | d1\startTrillSpan | d2\stopTrillSpan-"D.C. al Coda"
\bar "||"

\break



   % The coda
  \once \override Score.RehearsalMark.font-size = #4
   \mark \markup { \musicglyph #"scripts.coda" }

   
   b4\f b4 | c8( b) a b c4 c | c2 e4 e4 | fis8( e) d c fis4 fis | fis2 b,4 b |\break
   c8( b) a g g'4 r | r1 | r2 e8 d e fis | g fis g a b a b c | \break
   d4 r c8 b c a | b a b fis g fis g a | e4 r r2 | c1\startTrillSpan\p | b4\stopTrillSpan r4 r2
    \bar"|."

}
\header {
    piece = "Vivace"
}

}
