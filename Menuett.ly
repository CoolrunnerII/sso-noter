\header {
  title = "Menuett"
  composer = "?"
  copyright = "Tor Berg"
  instrument = "Viola"
  
}
\version "2.18.2"

\score 
{
\relative c'
{ 
\time 3/4
\clef alto
\key g \major
\set Score.markFormatter = #format-mark-circle-numbers
\compressFullBarRests
\partial 4
r4 |

\repeat volta 2 {
    b c( d) | e fis e8( d) | c4 d e | fis g^"'" a | b a^.( g^.) | fis e^.( d^.) \break
    c2 c4 | d b d, | g8( a) b 4 c 
}
  fis g d | fis d es | es as, as | \break
  as es' g | g c, c | d g g | g g g | g e c | b2 a8( b) | \break
  c4 d e | d c b | c <c e>2\upbow( | <c e>4) r\fermata c^\markup { \center-align {\italic pizz. }} | d r_"Fine" \bar"||" r^"Trio" | r8^\markup { \center-align {\italic arco }} b c4( b) | \break
  r8 b([ c b)] c( b) | r c d4( c) | r8 c([ d c)] b( g) | c,([ g' c b)] c( g) |\break
  r e([ cis' b)] cis8( e,) | r c' d4( c) | b8 d, g4 \bar"||" r^\markup { \center-align {\italic pizz. }} | c r r | c r r | \break
  c r r | d r r^\markup { \center-align {\italic arco }} | r8 gis,\downbow[ a gis(] gis) gis | r a[ b a(] a) a | r ais[ b ais]( ais) ais | \break
  r b cis4 b | r8 b c4( b) |r8 b[( c b]) c( b) | r c d4( c) | r8 c([ d c]) b( g) | \break
  c,([ g' c b)] c( g) | r e([ cis' b)] cis e | r c d4( c) | b8 d,_"D.C. al Fine" g4 \bar"||"
    
}
\header {
    piece = ""
}

}
